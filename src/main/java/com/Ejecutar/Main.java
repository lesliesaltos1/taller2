package Com.Ejecutar;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;



import Com.Entidades.Clientes;
import Com.Entidades.Pais;


public class Main {
	static final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
			.configure() // configures settings from hibernate.cfg.xml
			.build();
	
	static SessionFactory sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();

	public static void main(String[] args) {
		Clientes cliente= new Clientes(0,"leslie ","Saltos","Ortiz");
		ingresarClientes(cliente);
		Clientes cliente1= new Clientes(1,"kiki ","bella","amor");
		ingresarClientes(cliente1);
		
		Pais pais= new Pais(0,"Paris ");
		ingresarpais(pais);
		Pais pais1= new Pais(1,"espa�a ");
		ingresarpais(pais1);
		
		
		
	

	}
	static void ingresarClientes(Clientes cliente) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(cliente);
		session.getTransaction().commit();
		session.close();
	}
	
	static void ingresarpais(Pais pais) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(pais);
		session.getTransaction().commit();
		session.close();

	}
	   static List<Clientes> getCliente(){
			Session session = sessionFactory.openSession();
			List<Clientes> pais=(List<Clientes>)session.createQuery("from cliente",Clientes.class ).list();
			return pais;
		}


}
