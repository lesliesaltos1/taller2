package Com.Entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;



@Entity
public class Clientes {
	
	@Id
	@GeneratedValue
	
	private int id;
	private String nombres;
	private String apellidosp;
	private String apellidom;
	
	
	@OneToOne
	private Pais pais;

	
	
	public Clientes() {
		super();
	}
	public Clientes(int id, String nombres, String apellidosp, String apellidom) {
		super();
		this.id = id;
		this.nombres = nombres;
		this.apellidosp = apellidosp;
		this.apellidom = apellidom;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidosp() {
		return apellidosp;
	}
	public void setApellidosp(String apellidosp) {
		this.apellidosp = apellidosp;
	}
	public String getApellidom() {
		return apellidom;
	}
	public void setApellidom(String apellidom) {
		this.apellidom = apellidom;
	}
	

	
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	@Override
	public String toString() {
		return "Clientes [id=" + id + ", nombres=" + nombres + ", apellidosp=" + apellidosp + ", apellidom=" + apellidom
				+ "]";
	}
	
	

}
