package Com.Entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Pais {
	@Id
	@GeneratedValue

	private int id;
	private String nombrepais;
	
	
	@OneToMany(mappedBy ="pais")
	private List<Clientes> pais = new ArrayList<Clientes>();
	
	
	public Pais() {
		super();
	}
	public Pais(int id, String nombrepais) {
		super();
		this.id = id;
		this.nombrepais = nombrepais;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombrepais() {
		return nombrepais;
	}
	public void setNombrepais(String nombrepais) {
		this.nombrepais = nombrepais;
	}
	

	public List<Clientes> getPais() {
		return pais;
	}
	public void setPais(List<Clientes> pais) {
		this.pais = pais;
	}
	@Override
	public String toString() {
		return "Pais [id=" + id + ", nombrepais=" + nombrepais + "]";
	}
	
	
	
	
	
	
	
	
}
